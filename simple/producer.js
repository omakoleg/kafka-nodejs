const { Kafka } = require("kafkajs");
const {randomUUID } = require("crypto");

const kafka = new Kafka({
  clientId: "my-app-default",
  brokers: ["localhost:29092"],
});

async function main() {
  const producer = kafka.producer();
  await producer.connect();
  await producer.send({
    topic: "test-topic-default",
    messages: [
      { key: "a1", value: JSON.stringify({ test: "two" }) },
      { key: "a2", value: JSON.stringify({ test: "three" }) },
    ],
  });
  await producer.disconnect();
}
main();
