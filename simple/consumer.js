const { Kafka } = require("kafkajs");

const kafka = new Kafka({
  clientId: "my-app",
  brokers: ["localhost:29092"],
});

async function main() {
  const consumer = kafka.consumer({ groupId: 'test-group-2' })
  await consumer.connect()
  await consumer.subscribe({ 
    topic: 'test-topic', 
    fromBeginning: true,
   })
  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      console.log({
        key: message.key.toString(),
        value: message.value.toString(),
      })
    },
  })
}
main();
