#!/usr/bin/env bash

set -euo pipefail
MYDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$MYDIR/"


kcat \
  -b localhost:29092 \
  -t topic-1-local \
  -k some-key
  -P "./message-1.json"
