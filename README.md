## Kafka setup

To run Kafka and Kafka UI locally, use `docker-compose.yaml`:

```sh
docker-compose up
```

Some valuable ENV variables:

- `KAFKA_LISTENERS` are for internal use, define 3 parts: internal comms, controller and external.
- `KAFKA_LISTENER_SECURITY_PROTOCOL_MAP` maps all security to `PLAINTEXT`.
- `KAFKA_ADVERTISED_LISTENERS` used by clients to connect.

Access from host (mac):
`kcat -b localhost:29092 -L`

Access from docker-compose network:
Kafka UI is connecting to the internal docker network by `KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS: kafka:9092`. Kafka container exposing `9092:9092` for that.


Kafka UI is available on host under `http://localhost:8080/`.

### Producing/consuming messages in lLocally running kafka

Folder `simple`:
- `consumer.js` sample consumer: `node consumer.js`
- `producer.js` sample cporducer: `node producer.js` publishing 2 messages
- `send-sample-kafka-message.sh` script publising `message-1.json` into the Kafka topic


# Parallel consumtion showacae

Folder `partitions`:
- `producer-partitions.js` producer, creating topic with 8 partitions and publishing 200_000 messages
- `consumers.json` pm2 config file
- `consumer-partitions.js` dummy consumer with 1ms processing delay per message

Published messages:
- key: `uuid`
- value: `i_<random-string>`

Start paralle consumers:
```sh
yarn pm2 start consumers.json --attach
```
