const { Kafka } = require("kafkajs");
const { randomUUID, randomBytes } = require("crypto");

const kafka = new Kafka({
  clientId: "my-app",
  brokers: ["localhost:29092"],
});

const topicName = "partitions-8"

async function createTopic() {
  const admin = kafka.admin();
  await admin.connect();
  console.log({ topics: await admin.listTopics() });
  await admin.createTopics({
    topics: [
      {
        topic: topicName,
        numPartitions: 8,
      },
    ],
  });
  console.log({ topics: await admin.listTopics() });
  await admin.disconnect();
}

async function fetchTopicOffsets() {
  const admin = kafka.admin();
  await admin.connect();
  console.log({ topics: await admin.listTopics() });
  console.log({ topics: await admin.fetchTopicOffsets(topicName) });
  await admin.disconnect();
}

async function produceMessages() {
  const producer = kafka.producer();
  await producer.connect();
  for (let i = 0; i < 200000; i++) {
    if(i % 10000 == 0) {
      console.log(i)
    }
    const value = `${i}_${randomBytes(20).toString('hex')}`;
    const key = randomUUID();
    await producer.send({
      topic: topicName,
      messages: [
        { key, value },
      ],
    });
  }
  await producer.disconnect();
}

async function main() {
  await createTopic();
  await fetchTopicOffsets();
  await produceMessages();
  await fetchTopicOffsets();
}
main();
