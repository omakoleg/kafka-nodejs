const { Kafka } = require("kafkajs");
const kafka = new Kafka({
  clientId: "my-app",
  brokers: ["localhost:29092"],
});
const topicName = "partitions-8";
const groupId = "group-16parallel-3";
let readTime = new Date().getTime() + 20 * 1000; // some seconds for Warmup
async function main() {
  console.log(`[${new Date().toUTCString()}] Starting consumer ${process.pid}`);
  const consumer = kafka.consumer({ groupId });
  await consumer.connect();
  await consumer.subscribe({
    topic: topicName,
    fromBeginning: true,
  });
  await consumer.run({
    autoCommit: true,
    autoCommitInterval: 1000,
    eachMessage: async ({ topic, partition, message }) => {
      readTime = new Date().getTime();
      // process message
      console.log(`${partition} => ${message.value.toString()}`);
      await new Promise((res) => setTimeout(res, 1)); // delay
    },
  });
}

setInterval(() => {
  const now = new Date().getTime();
  const diffMs = now - readTime;
  console.log({ diffMs });
  if (diffMs > 1000) {
    console.log(
      `[${new Date().toUTCString()}] Exiting consumer ${process.pid}`
    );
    process.exit(0);
  }
}, 1000);

main();
